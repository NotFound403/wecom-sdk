/*
 * Copyright (c) 2024. felord.cn
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       https://www.apache.org/licenses/LICENSE-2.0
 * Website:
 *       https://felord.cn
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.felord.domain.wedoc.smartsheet;

import cn.felord.enumeration.SheetFieldType;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The type Text sheet field.
 *
 * @author dax
 * @since 2024 /12/23
 */
public class EmailSheetField extends SheetField {


    /**
     * Create progress sheet field.
     *
     * @param fieldTitle the field title
     * @return the progress sheet field
     */
    public static EmailSheetField create(String fieldTitle) {
        return update(null, fieldTitle);
    }

    /**
     * Update progress sheet field.
     *
     * @param fieldId    the field id
     * @param fieldTitle the field title
     * @return the progress sheet field
     */
    public static EmailSheetField update(String fieldId, String fieldTitle) {
        return new EmailSheetField(fieldId, SheetFieldType.FIELD_TYPE_EMAIL, fieldTitle);
    }

    /**
     * Instantiates a new Sheet field.
     *
     * @param fieldId    the field id
     * @param fieldType  the field type
     * @param fieldTitle the field title
     */
    @JsonCreator
    EmailSheetField(@JsonProperty("field_id") String fieldId,
                    @JsonProperty("field_type") SheetFieldType fieldType,
                    @JsonProperty("field_title") String fieldTitle) {
        super(fieldId, fieldType, fieldTitle);
    }
}
